
import {Fragment} from 'react';
import {useState, useEffect} from'react';
import {Container} from 'react-bootstrap';
import AppNavbar from './components/AppNavbar';
import{BrowserRouter as Router} from 'react-router-dom';
import {Route, Switch} from 'react-router-dom';


import {UserProvider} from "./user-context"
// import {ProductProvider} from "./product-context"
import Home from './pages/Home';
import Products from './pages/products';
import SignUp from './pages/signup';
import Login from './pages/login';

import './App.css';
import Logout from './pages/logout';
import Cart from './pages/cart';
import ProductView from './pages/product-view';
import Profile from './pages/user-dashboard';
import AddProduct from './pages/add-product';
import UpdateProduct from './pages/update-product';
import Error from './pages/error';
import AdminDashboard from './pages/admin-dashboard';
import Review from './pages/add-review';
import UserOrders from './pages/user-orders';



// enclose all your components inside fragments for them to be usable
function App() {
  const [user, setUser]=useState({id:null, isAdmin:null })

  const unsetUser = ()=>{localStorage.clear();}
    useEffect(()=>{
      
      console.log(localStorage);
    }, [user])


  return (
            <UserProvider value={{user, setUser, unsetUser}}>
            <Router>
            <Switch>
             <Route exact path ="/" component ={Home} />
            <div className="p-0 m-0 ">
                <AppNavbar />
                <Container className="pr-0 pl-0 ">
                  <Switch>
                     
                     <Route exact path ="/products/all" component ={Products} />
                     <Route exact path ="/products/add" component ={AddProduct} />
                     <Route exact path ="/products/:productId/update" component ={UpdateProduct} />
                     <Route exact path ="/products/:productId" component ={ProductView} />
                     <Route exact path ="/cart" component ={Cart} />
                     <Route exact path ="/login" component ={Login} />
                     <Route exact path ="/logout" component ={Logout} />

                     <Route exact path ="/profile" component ={Profile} />
                     <Route exact path ="/profile/orders" component ={UserOrders} />

                     <Route exact path ="/:productId/review" component ={Review} />
                     <Route exact path ="/admin" component ={AdminDashboard} />
                     <Route exact path ="/register" component ={SignUp} />
                     <Route exact path ="/error" component ={Error} />
                  </Switch>
                </Container>
                </div>
              </Switch>
            </Router>
            </UserProvider>


  );
}

export default App;


