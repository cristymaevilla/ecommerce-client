
import { Fragment, useContext, useState, useEffect } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../user-context';
import Badge from 'react-bootstrap/Badge'

import '../App.css';

// AppNavbar component
export default function AppNavbar(){
	const{user} =useContext(UserContext);
	let[items, setItems]= useState([]);
	useEffect(()=>{
		
		console.log(user.id);
		console.log(user.isAdmin);
		console.log(user);
		console.log(`useraccess ${user.access}`);
		console.log(localStorage.token)
		fetch('https://floating-beach-02947.herokuapp.com/cart', {
		    headers: {
		        Authorization: `Bearer ${ localStorage.token }`
		    }
		    })
		.then (res=> res.json())
		.then(data =>{
			console.log(data);
				setItems(data);	
				
		})
	}, [])

function countItems(){
	
	console.log(`user${user.isAdmin}`);
	console.log(`useraccess ${user.access}`);
	console.log(localStorage.token)
	fetch('https://floating-beach-02947.herokuapp.com/cart', {
	    headers: {
	        Authorization: `Bearer ${ localStorage.token }`
	    }
	    })
	.then (res=> res.json())
	.then(data =>{
		console.log(data);
			setItems(data);	
			
	})
}
	return (
		(user.isAdmin) ?
			  <Navbar className=" fixed-top white "expand="lg">
		      <Navbar.Brand className="montagu dark-blue-text" as={NavLink} to="/" exact>Majestea</Navbar.Brand>
			      <Navbar.Toggle aria-controls="basic-navbar-nav" />
			      <Navbar.Collapse id="basic-navbar-nav" className="border-0 rounded-0">
			        <Nav className="ml-auto">
			         		<Nav.Link as={NavLink} to="/products/all" exact>Products</Nav.Link>
			          		<Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link> 
			          		<Nav.Link as={NavLink} to="/admin" exact>Dashboard</Nav.Link> 	
			        </Nav>
			      </Navbar.Collapse>
			 
			  </Navbar>

		:
		  <Navbar className=" fixed-top white "expand="lg">
	      <Navbar.Brand className="montagu" as={NavLink} to="/" exact>Majestea</Navbar.Brand>
		      <Navbar.Toggle aria-controls="basic-navbar-nav" />
		      <Navbar.Collapse id="basic-navbar-nav" className="border-0 rounded-0">
		        <Nav className="ml-auto">
		          <Nav.Link as={NavLink} to="/products/all" exact onClick={countItems} >Shop</Nav.Link>
		          {(user.id !== null) ?
		          	<>
		          		<Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link> 
		          	 	<Nav.Link as={NavLink} to="/profile" exact>Me</Nav.Link> 
		          	 	<Nav.Link as={NavLink} to="/cart/" exact onClick={countItems}>Cart<Badge variant="primary">{items.length}</Badge>
		          	 	</Nav.Link>
					 </>
		          :
		          	<Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link> 
		          }
		        </Nav>
		      </Navbar.Collapse>
		 
		  </Navbar>
		)
}
