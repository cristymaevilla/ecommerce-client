import PropTypes from 'prop-types'
import { MdOutlineDeliveryDining, MdTaskAlt } from "react-icons/md";
import {Link} from 'react-router-dom';
import {useState, useContext } from 'react';
import UserContext from '../user-context';
import { Button, Card, Row, Col }from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function OrderCard({orderProp}){
	
	const {user, setUser}=useContext(UserContext);
	const{id, purchasedOn, shippingFee, subTotal, status, totalAmount, paymentMethod}=orderProp;
	const[isDelivered, setIsDelivered]= useState(false);
	const[status2, setStatus2]= useState("TO SHIP");


function update(){
			console.log(orderProp._id);
			fetch(`https://floating-beach-02947.herokuapp.com/orders/${orderProp._id}/status`, {
			    method: 'PUT',
			     headers: {
			         'Content-Type': 'application/json',
			         Authorization: `Bearer ${ localStorage.token }`
			               },
			   body: JSON.stringify({
			         status: "delivered",
			    })
			    })
			.then (res=> res.json()).then(data =>{
				if(data !== false ){
					setIsDelivered(true);
					setStatus2("DELIVERED");
					const Toast = Swal.mixin({
					  toast: true,
					  position: 'top-end',
					  showConfirmButton: false,
					  timer: 3000,
					  timerProgressBar: true,
					  didOpen: (toast) => {
					    toast.addEventListener('mouseenter', Swal.stopTimer)
					    toast.addEventListener('mouseleave', Swal.resumeTimer)
					  }
					})

					Toast.fire({
					  icon: 'success',
					  title: 'Updated in successfully'
					})
				}
				
			})
		}

	
	return( 
			(user.isAdmin) ?

				<Col className="mt-2 mb-2 p-0  col-12 d-flex-column justify-content-center">
				<Card className=" p-0 col-12 shadow1">
				  <Card.Header className=" l-blue t-blue montagu  text-blue"><em>Order Id : {orderProp._id}</em></Card.Header>
				  <Card.Body className=" pl-2 pr-2 ml-2 mr-2 d-md-flex">
				  <div className=" col-md-6 text-left"> 
				  	<p className="mb-0">Purchased on:  </p>
				  	<p className="mb-0"> {purchasedOn} </p>
				  	<p className="mb-0">Shipping Fee: {shippingFee}</p>
				  	<p className="mb-0">Payment Method : {paymentMethod}</p>
				  </div>
				  <div className=" col-md-4 text-left"> 
				    <p>SubTotal: {subTotal}</p>
				    <p><b>Total Amount: {totalAmount}</b></p>
				   </div>
				   <div>
				   	{(status === "delivered") ?
				   		<p  className=" text-center mb-0 text-blue"><b>STATUS: Delivered <MdTaskAlt/>  </b></p >
				   	  : <>
				   	  	<p className="text-center mb-0 text-blue" >{status2}</p >
				   	  	{isDelivered? 
				   	  		<span className="updated rounded float-right col-12 text-center align-middle p-1 mt-4 ml-2">
				   	  				<span><MdTaskAlt/> </span>		
				   	  		</span>
				   	  		:
				   	  		<span onClick={update} className="add-button float-right col-12 text-center align-middle p-1 mt-4 ml-2">
				   	  				<span>UPDATE</span>		
				   	  		</span>
				   	  	}
				   	  
				   	  	</>
				   	   }
				   		
				   		 
				   	
				   </div>
				
				
				  </Card.Body>

				</Card>
				</Col >

			:
				<Col className="mt-2 mb-2 p-0 col-12 d-flex-column justify-content-center">
				<Card className=" p-0 col-12 shadow1">
				  <Card.Header className=" l-blue t-blue montagu  text-blue"><em>Order Id : {orderProp._id}</em></Card.Header>
				  <Card.Body className=" pl-2 pr-2 ml-2 mr-2 d-md-flex">
				  <div className=" col-md-6 text-left"> 
				  	<p className="mb-0">Purchased on:  </p>
				  	<p className="mb-0"> {purchasedOn} </p>
				    <p className="mb-0">Shipping Fee: {shippingFee}</p>
				    <p className="mb-0">Payment Method : {paymentMethod}</p>
				  </div>
				  <div className=" col-md-4 text-left"> 
				    <p>SubTotal: {subTotal}</p>
				    <p><b>Total Amount: {totalAmount}</b></p>
				   </div>
				   
				   <div className=" text-center t-blue">
				           STATUS: {status}
				    </div>
				
				  </Card.Body>

				</Card>
				</Col >

				
			)
	}


		OrderCard.propTypes = {

		order:PropTypes.shape({
			purchasedOn:PropTypes.string.isRequired,
			id:PropTypes.string.isRequired,
			paymentMethod: PropTypes.string.isRequired,
			shippingFee: PropTypes.number.isRequired,
			subTotal: PropTypes.number.isRequired,
			totalAmount: PropTypes.number.isRequired,
			status: PropTypes.string.isRequired
		
		})
	}