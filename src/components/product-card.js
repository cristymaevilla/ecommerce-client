import {useState, useContext } from 'react';
import Swal from 'sweetalert2';
import PropTypes from 'prop-types'
import {  Card, }from 'react-bootstrap';
import Image from 'react-bootstrap/Image'
import productImage from '../images/product.png'; 
import { FaCheck } from 'react-icons/fa';
import { Link, NavLink, useHistory } from 'react-router-dom';
import UserContext from '../user-context';



export default function ProductCard({productProp}, cartCount){
const[productDetails, setProductDetails]= useState({});
let history = useHistory();
const{id, name, description, stocks, price, isActive, src}=productProp;
const {user, setUser}=useContext(UserContext);
const[items, setItems]= useState(0);
const[quantity, setQuantity]= useState(1);
const[full, setFull]= useState(false);
const[isAdded, setIsAdded]= useState(false);

function increment(){
	if(quantity === 20){setQuantity(20)}
	else{setQuantity(quantity +1);}}
function decrement(){
	if(quantity === 1){setQuantity(1)}
	else{setQuantity(quantity-1);}}

function getProductDetails(){
	fetch(`https://floating-beach-02947.herokuapp.com/products/${productProp._id}`)
	.then (res=> res.json())
	.then(data =>{
		console.log(data);
		console.log(quantity);
		setProductDetails(data);
		console.log({
	                   productDetails
	                })

	})
}

function subtotal(a,b){return a*b};

function fetchItems(){
	console.log(user.id)
	if (user.id === null){
		Swal.fire({
			          title: `Oops. I beg your pardon`,
			          icon: 'warning',
			          text: 'Kindly Login first.'   
			      });
	}else{
	console.log(user.id);
	console.log(`useraccess ${user.access}`);
	console.log(localStorage.token)
	
fetch('https://floating-beach-02947.herokuapp.com/cart', {
	    headers: {
	        Authorization: `Bearer ${ localStorage.token }`
	    }
	    })
	.then (res=> res.json())
	.then(data =>{
		console.log(data);
		if (data.length > 9){
			Swal.fire({
				       title: `Good gracious, your cart is full.`,
				       icon: 'warning',
				       text: 'Check out now!'   
				   });
		 }else{addToCart()}

		
	})


}

}




function addToCart() {
	console.log(user)

		if (stocks < quantity){
			Swal.fire({
				          title: `Good gracious, ${stocks} stocks left!`,
				          icon: 'warning',
				          text: 'Please change the quantity of your item.'   
				      });
		}
		else{


				setIsAdded(true);
				fetch(`https://floating-beach-02947.herokuapp.com/cart/${productProp._id}/addtocart`, {
			    method: "PUT",
			    headers: {
			    	// AUTH FAILED ALWAYS
			    	'Content-Type': 'application/json',
			    	Authorization: `Bearer ${ localStorage.token }`
			          
			    },
			    body: JSON.stringify({
			        userId: user.id,
			        product: productProp._id,
			        name: name,
			        quantity: quantity,
			        stocks:stocks,
			        price: price,
			        subTotal: subtotal(price,quantity)
			    })
			})
			.then(res => res.json())
			.then(data => {
				console.log(data); 
			})
		}
}
// ----------------------------------------------------------------------------------
const[isActivated1, setIsActivated1]= useState("ACTIVATED");
const[color1, setColor1]= useState("add-button");
const[isActivated2, setIsActivated2]= useState("DEACTIVATED");
const[color2, setColor2]= useState("deactivated");

function archive(){
			fetch(`https://floating-beach-02947.herokuapp.com/products/${productProp._id}/archive`, {
					method: "PUT",
				    headers: {
				        Authorization: `Bearer ${ localStorage.token }`
				    }
				    })
				.then (res=> res.json())
				.then(data =>{
					console.log(data);
					if (data === true){
						setIsActivated1("DEACTIVATED");
						setColor1("deactivated")
		
					
						Swal.fire({
							       title: `Product deactivated.`,
							       icon: 'success',
	  
							   });
					 }else{
					 	Swal.fire({
					 		       title: `Something went wrong`,
					 		       icon: 'warning',
					 		       text: 'Please try again'   
					 		   });
					 }

					
				})
			}
					
function activate(){
			fetch(`https://floating-beach-02947.herokuapp.com/products/${productProp._id}/activate`, {
					method: "PUT",
				    headers: {
				        Authorization: `Bearer ${ localStorage.token }`
				    }
				    })
				.then (res=> res.json())
				.then(data =>{
					console.log(data); setIsActivated2("ACTIVATED"); 

					history.push("/products/all");
					if (data === true){
						
						Swal.fire({
							       title: `Product activated`,
							       icon: 'success',  
							   }); 	
						history.push("/products/all");
						
				
					 }else{
					 	Swal.fire({
					 		       title: `Something went wrong`,
					 		       icon: 'warning',
					 		       text: 'Please try again'   
					 		   });
					 }

					
				})
			}

	return( 
	
		
		(user.isAdmin) ?	
		<div className=" col-12 col-sm-6 col-md-4 col-lg-3 container-fluid pl-0 pr-0 ">	
			<div className="m-0 p-0 border-0 text-center text-sm-left">
			  <Card.Body className="p-0">
				<div className="container-fluid p-0 bg-none">
					<Link className	="none" to={`/products/${productProp._id}`}>
						<Image  onClick={getProductDetails} src={src} fluid className="zoom-out"/>
					</Link>

				</div>
			    <h5 className="pl-sm-3 pl-xl-4 text-blue ">{name}</h5>

			    <div className="d-sm-flex pl-sm-3 pl-xl-4">
			    	<h5 className="mb-0 text-blue poppins">Php {price}</h5> 
			    	<p className="p-0 m-0 pl-sm-4">20 tea bags</p>
			    </div>
			    	<p className="p-0 m-0 pl-sm-3 pl-xl-4">Stocks: {stocks}</p>
			 
			    
			    <div className="d-flex justify-content-center container-fluid p-0 mr-md-0">
			    	<Link className	=" col-5 add-button align-middle pt-1 ml-2 text-center" to={`/products/${productProp._id}/update`}>
			    		
			    			<span>UPDATE </span>		
			    		
			    	</Link>
						
			    {(isActive == true)? 
			    	<span onClick={archive} className="add-button status-text text-center  pl-0 pr-0 col-5 align-middle pt-1 ml-2">
			    		<span>{isActivated1}</span>		
			    	</span>				    	
			    : 
			    	<span onClick={activate} className="add-button status-text text-center  pl-0 pr-0 col-5 align-middle pt-1 ml-2">
			    	 		<span>{isActivated2}</span>		
			    	 </span> 
			    }

			    </div>
			  </Card.Body>
			</div>
		</div>
:
			

			<div className=" col-12 col-sm-6 col-md-4 col-lg-3 container-fluid pl-0 pr-0 ">	
				<div className="m-0 p-0 border-0 text-center text-sm-left">
				  <Card.Body className="p-0">
					<div className="container-fluid p-0 bg-none">
						<Link className	="none" to={`/products/${productProp._id}`}>
							<Image  onClick={getProductDetails} src={src} fluid className="zoom-out"/>

						</Link>
					</div>
				    <Card.Subtitle className="pl-sm-3 pl-xl-4">{name}</Card.Subtitle>

				    <div className="d-sm-flex pl-sm-3 pl-xl-4">
				    	<h5 className="mb-0">Php {price}</h5> 
				    	<p className="p-0 m-0 pl-sm-4">20 tea bags</p>
				    </div>
				    	<p className="p-0 m-0 pl-sm-3 pl-xl-4">Stocks: {stocks}</p>
				    
				    <div className="d-flex justify-content-center container-fluid p-0 mr-md-0">
							<span className="item2 p-0 text-center" onClick={decrement}>-</span>
				    		<span className="item1  p-0 text-center ">{quantity}</span>
				    		<span className="item2 p-0 text-center" onClick={increment}>+</span>
				    {isAdded ? 
				    	<span  className="add-button text-center col-6 align-middle pt-1 ml-2">
				    		<span>ADDED <FaCheck/></span>		
				    	</span>				    	
				    : 
				    	<span onClick={fetchItems} className="add-button text-center col-6 align-middle pt-1 ml-2">
				    		<span>ADD</span>		
				    	</span>
				    }

				    </div>
				  </Card.Body>
				</div>
			</div>

		)
}

// checks the validity of proptypes
ProductCard.propTypes = {
	// shape()-if prop object conforms with the specific shape
	product:PropTypes.shape({

		name:PropTypes.string.isRequired,
		src:PropTypes.string.isRequired,
		stocks:PropTypes.number.isRequired,
		price: PropTypes.number.isRequired
	})
}