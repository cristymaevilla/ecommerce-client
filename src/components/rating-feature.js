import {useState} from 'react';
import Carousel from 'react-bootstrap/Carousel';
import Image from 'react-bootstrap/Image'
import f1 from '../images/f1.png'; 
import f2a from '../images/f2-a.png'; 
import f2b from '../images/f2-b.png'; 
import f2c from '../images/f2-c.png'; 
import f3 from '../images/f3.png'; 

import { FaStar } from "react-icons/fa";
const colors = {
    blue: " #007BFF",
    darkblue: "#051835"
 };
export default function NotFound(){
const stars = Array(5).fill(0)	
		return (
		<div className="d-sm-flex col-12 justify-content-center m-0 p-0">
			<div className="order-1 order-sm-2 pt-5 d-flex-column align-items-middle">
				<h1 className="montagu  d-flex text-center text-blue  rating-text">The Royalties have spoken</h1>
				<h1 className="d-flex p-0 mt-4 float-center justify-content-center align-items-middle rating-text">
				  {stars.map((_, index) => {
				    return (
				      <FaStar
				        key={index}
				        size={19}
				        color={5 > index ? colors.blue : colors.darkblue}
				        style={{
				          marginRight: 10,
				        }}
				      />
				    )
				  })}
				</h1 >
				
			</div>
		<div className="d-flex col-sm-5 col-12 justify-content-center margin-auto p-0 m-0 overflow-hidden order-2 order-sm-1">
			<Image src={f3} fluid className=" p-0 m-0 position-absolute col-12 "/>
			
			<Carousel className="col-12 p-0 m-0 overflow-hidden position-absolute"  >

			    <Carousel.Item className="  m-0">
			     	<Image src={f2a} fluid className=" p-0 m-0 col-12  m-0 w-100 "/>
			    </Carousel.Item>

			    <Carousel.Item className="  m-0">
			     	<Image src={f2b} fluid className=" p-0 m-0 col-12  m-0 w-100 "/>
			    </Carousel.Item>

			    <Carousel.Item className="  m-0">
			     	<Image src={f2c} fluid className=" p-0 m-0 col-12  m-0 w-100 "/>
			    </Carousel.Item>

			</Carousel>
			<Image src={f1} fluid className=" p-0 m-0  col-12 position-absolute "/>
			<Image src={f1} fluid className=" p-0 m-0  col-12 "/>
		</div>
		</div>
			)
	
}





	

			


		

	