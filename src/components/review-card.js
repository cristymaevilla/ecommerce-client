import PropTypes from 'prop-types'
import {Link} from 'react-router-dom';
import {useState, useContext } from 'react';
import UserContext from '../user-context';
import { Button, Card, Row, Col }from 'react-bootstrap';
import Swal from 'sweetalert2';
import { FaStar } from "react-icons/fa";
const colors = {
    blue: " #007BFF",
    none: "#cbd0d3"
 };

export default function ReviewCard({reviewProp}){
	
	const {user, setUser}=useContext(UserContext);
	const{id, product, rating, comment, name}=reviewProp;
	// const[rating, setRating]= useState(4);
	const stars = Array(5).fill(0)

	
	return( 
	(user.isAdmin) ?
				<Row className=" col-12 ml-0 mr-0 d-flex justify-content-center">
					<Card className=" p-0 col-11 shadow1 mb-2">
						<Card.Body className=" pl-2 pr-2 ml-2 mr-2 d-sm-flex">
						  <div className="col-sm-4 col-12 text-left"> 
						  	<h5 className="mb-0 text-blue montagu"><b>{reviewProp.name}</b></h5>
						  		<span className="d-flex p-0 m-0 align-items-middle">
						  		  {stars.map((_, index) => {
						  		    return (
						  		      <FaStar
						  		        key={index}
						  		        size={15}
						  		        color={{rating} > index ? colors.blue : colors.none}
						  		        style={{
						  		          marginRight: 8,
						  		        }}
						  		      />
						  		    )
						  		  })}
						  		</span>
						  </div>
						 <p className="mb-0 col-sm-6 col-12">"{comment}"</p>
						 <p className=" col-sm-2 col-12 ml-1 blue text-center rounded p-1 sub-link">Feature</p>
						</Card.Body>
					</Card>
				</Row>
			:
			
			<Card className=" p-0 col-11 col-md-5 shadow1 mb-2 pb-0 ml-2">
				<Card.Body className=" pl-2 pr-2 ml-2 mr-2 d-md-flex pb-0">
				  <div className="col-12 text-center"> 
				  	<h5 className="mb-0 text-blue montagu"><b>{product}</b></h5>
				  	<span className="mb-0"><b>Rating</b>
				  		<span className="d-flex p-0 m-0 float-center justify-content-center align-items-middle">
				  		  {stars.map((_, index) => {
				  		    return (
				  		      <FaStar
				  		        key={index}
				  		        size={15}
				  		        color={rating > index ? colors.blue : colors.none}
				  		        style={{
				  		          marginRight: 8,
				  		        }}
				  		      />
				  		    )
				  		  })}
				  		</span>
				  	</span>
				  	<p className="mb-0"><b>Comment</b></p>
				  	<p className="mb-0">"{comment}"</p>
				  	<div className="d-flex justify-content-center mb-0">
				  		<p className=" col-sm-4 col-5 ml-1 blue text-center rounded p-1  sub-link">Remove</p>
				  		<p className=" col-sm-4 col-5 ml-1 blue text-center rounded p-1 sub-link">Update</p>						
				  	</div>
				  </div>
				</Card.Body>
			</Card>

				
			)
	}


		ReviewCard.propTypes = {

		review:PropTypes.shape({
			id:PropTypes.string.isRequired,
			product: PropTypes.string.isRequired,
			name: PropTypes.string.isRequired,
			rating: PropTypes.number.isRequired,
			comment: PropTypes.string.isRequired
		
		})
	}