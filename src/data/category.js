const categoryData=[
	{
		id: '1',
		name: 'GREEN',
		param:'greenTea',
		description: 'Green tea is exceptionally high in flavonoids that can help boost your heart health by lowering bad cholesterol and reducing blood clotting.'
	},
	{
		id: '2',
		name: 'BLACK',
		param:'blackTea',
		description: 'The flavors found in black teas include smoky, earthy, spicy, nutty, citrus, caramel, leather, fruity, and As opposed to green tea, which is light and grassy, black tea has a bolder, sweeter flavor.'
	},
	{
		id: '3',
		name: 'FRUIT',
		param:'fruitTea',
		description: ' Delicate, fragrant, and oh-so-pure, our fruit teas provide the perfect boost. From fruity and floral to decadent blends with a hint of something sweet. Add a natural boosting brew to your day'
	},
	{
		id: '4',
		name: 'HERBAL',
		param:'herbalTea',
		description: 'Herbal teas contain a blend of herbs, spices, fruits or other plants in addition to tea leaves. Herbal teas don’t contain caffeine, which is why they’re known for their calming properties.'
	}

]
export default categoryData;