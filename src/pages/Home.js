import {Fragment} from 'react';
import Nav from 'react-bootstrap/Nav';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import { Button , Row , Col}from 'react-bootstrap';
import { NavLink} from 'react-router-dom'; 
import Image from 'react-bootstrap/Image'
import Rating from '../components/rating-feature'
// IMAGES--------------------------------
import product1a from '../images/lp-product1-a.png'; 
import product1b from '../images/lp-product1-b.png'; 
import product1c from '../images/lp-product1-c.png'; 
import slogan from '../images/slogan1.svg'; 
import footer from '../images/footer.jpg'; 
import fruit1 from '../images/fruit1.png';
import fruit2 from '../images/fruit2.png';
import black1 from '../images/black1.png';
import black2 from '../images/black2.png';
import herbal1  from '../images/herbal1.png';
import herbal2 from '../images/herbal2.png';
import green1 from '../images/green1.png';
import green2 from '../images/green2.png';



export default function Home(){
	return(
		<Col className=" text-middle  homepage-container vw-100 bg-secondary pl-0 pr-0">
		<div className="landingpage-bg1 vh-lg-100 dark-blue">
				<div className="col-12 d-flex d-md-none pl-0 pr-0 justify-content-around">
					<Nav.Link as={NavLink} to ="/products/all" exact className="p-0 pt-4" >
						<p className=" text-right pr-4 align-bottom poppins  text-left pl-0 text-white">SHOP</p> 
					</Nav.Link>
					<Nav.Link as={NavLink} to ="/login" exact className="p-0 pt-4 text-right" >
						<p className=" text-right pr-0 align-bottom poppins  text-right text-white">LOGIN</p> 
					</Nav.Link>
				</div>
				<h1 className="montagu product-name text-white text-center d-block d-md-none">MAJESTEA</h1>

				<div className="col-12 d-md-flex justify-content-around d-none pt-5">
					<Nav.Link as={NavLink} to="/products/all" exact  >
						<h5 className=" text-right pr-4 align-bottom poppins  home-link text-right ">SHOP</h5> <hr/>
					</Nav.Link>
					<h1 className="montagu product-name text-white">MAJESTEA</h1>
					<Nav.Link as={NavLink} to ="/login" exact  >
						<h5 className=" home-link text-right pr-4 align-bottom poppins   ">LOGIN</h5> 
					</Nav.Link>
				</div>

				<div className="container-fluid col-10 col-lg-5 col-md-8 margin-auto ">
					<Image src={product1b} fluid className="product rotate p-o m-0 position-absolute "/>
					<Image src={product1c} fluid className="product  p-0 m-0 position-absolute "/>
					<Image src={product1a} fluid className="product p-0 m-0 "/>
				</div>						
		</div>
		<div className="feature dark-blue pt-4"> 
			<h1 className="montagu footer-text-title text-center text-white">DRINK TEA LIKE A ROYAL</h1>
			<h3 className="text-center poppins text-white">Our teas are made from natural organic plants with a lot of health benefits.</h3>
			<div className="d-sm-flex justify-content-around m-0 p-0 ">
				<div className="col-sm-3 fruit d-flex justify-content-center ">
					<Image src={fruit2} fluid className="  position-absolute prod "/>
					<Image src={fruit1} fluid className="  fruit-con opacity-none "/>
				</div>
				<div className="col-sm-3  herbal d-flex justify-content-center">
					<Image src={herbal2} fluid className=" position-absolute prod"/>
					<Image src={herbal1} fluid className="herbal-con opacity-none"/>
				</div>
				<div className="col-sm-3  black1 d-flex justify-content-center">
					<Image src={black2} fluid className="  position-absolute prod"/>
					<Image src={black1} fluid className="  black-con opacity-none"/>
				</div>
				<div className="col-sm-3  green d-flex justify-content-center">
					<Image src={green2} fluid className="  position-absolute prod "/>
					<Image src={green1} fluid className="   green-con opacity-none"/>
				</div>		
			</div>
		</div>

		<div className="text-center d-flex-column col-12 justify-content-center dark-blue ">
			<Rating/>	
		</div>
		
		<div className="montagu footer-text-title pl-2 dark-blue text-white"> YOUR HIGHNESS... </div>
		<div className="footer dark-blue "> 
			<div className="position-absolute footer-text text-white ">
				<h5 className="poppins col-7 footer-text-subt">Get the lastest Majestea news and recieve royalTEA discounts!</h5>
				<h5 className=" montagu footer-link ml-3 pl-2 pr-2 subscribe mt-0 mt-md-4">SUBSCRIBE NOW!</h5> 
			</div>
			
			<div className="container-fluid col-12 m-0 p-0 pt-md-0 mt-sm-0">
					<Image src={footer} fluid className=" col-12 p-0 m-0 "/>

					
			</div>	
		</div>	
		</Col>	
				

		)
		
	
}
