import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../user-context.js';
import productImage from '../images/product.png'; 
import Image from 'react-bootstrap/Image'


export default function Register() {

    const {user} = useContext(UserContext);
    const history = useHistory();

    // State hooks to store the values of the input fields
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [category, setCategory] = useState('');
    const [price, setPrice] = useState('');
    const [stocks, setStocks] = useState('');
    const [src, setSrc] = useState('');
    const [isActive, setIsActive] = useState('');


    // Check if values are successfully binded

    // Function to simulate user registration
 function  add(e) {

        e.preventDefault();

        fetch('https://floating-beach-02947.herokuapp.com/products/add', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ localStorage.token }`
            },
    		body: JSON.stringify({
    		    name: name,
    		    description: description,
    		    category: category,
    		    price:price,
    		    src:src,
    		    stocks: stocks
    								})
    		})
    		.then(res => res.json())
    		.then(data => {
    			if(data === true){
    			    setName('');
    			    setDescription('');
    			    setCategory('');
    			    setPrice('');
    			    setSrc('');
    			    setStocks('');

    			    Swal.fire({
    			        title: 'Product Added',
    			        icon: 'success',
    			
    			    							});
    			    history.push("/products/all");

    			} 
    			else {
						Swal.fire({
    			        title: 'Something wrong',
    			        icon: 'error',
    			        text: 'Please try again.'   
    			    							});

    				};
    		});
       
}

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(name !== '' && description !== '' && category !== '' && src !== '' && price !== '' && stocks !== ''){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, description,category,  price, stocks]);
	
	return( 
	(user.isAdmin === true) ?
	<div className="m-0 pt-5  pb-4  addproduct-bg  ">
		<div className="d-md-flex">
			
			<div className="col-md-6 col-12 container-fluid order-md-2 pt-0 pt-md-5">

				 <Image   src={productImage} fluid className=""/>
			
			</div>
			<h5> </h5>
			<div className="col-md-6 col-12  order-md-1">
				<Form className="mt-5" onSubmit={(e) => add(e)}>
				  <Form.Group className="mb-2 pl-0" controlId="name">
				    <Form.Label>Name</Form.Label>
				    <Form.Control type="name"
				    	 placeholder="Name" 
				    	 value={name}
				    	 onChange={ e => setName(e.target.value)}
				    	 required
				    	 className="form-control rounded placeholder mb-2" />
				  </Form.Group>
				  
				   <Form.Group className="mb-2 pl-0" controlId="description">
					<Form.Label>Description</Form.Label>
				    <Form.Control type="description"
				    	 placeholder="Description" 
				    	 value={description}
				    	 onChange={ e => setDescription(e.target.value)}
				    	 required
				    	 className="form-control rounded placeholder mb-2" />
				  </Form.Group>

				  <Form.Group controlId="exampleForm.ControlSelect1">
				      <Form.Label>Category</Form.Label>
				    <Form.Control as="select"
				    	placeholder="Category" 
				    	value={category}
				    	onChange = { e => setCategory(e.target.value)}
				    	required 
				    	className="form-control rounded placeholder mb-2"
				    >
				      <option>green tea</option>
				      <option>black tea</option>
				      <option>herbal tea</option>
				      <option>fruit tea</option>
				    </Form.Control>
				  </Form.Group>

				  <Form.Group className="mb-2 pl-0" controlId="price">
					<Form.Label>Price</Form.Label>
				    <Form.Control type="price"  
				    	placeholder="Price" 
				    	value={price}
				    	onChange={ e => setPrice(e.target.value)}
				    	required 
				    	className="form-control rounded  placeholder mb-2" />
				  </Form.Group>

				  <Form.Group className="mb-2 pl-0" controlId="stocks">
				  <Form.Label>Stocks</Form.Label>
				  <Form.Control type="stocks"  
				    	placeholder="Stocks" 
				    	value={stocks}
				    	onChange={ e => setStocks(e.target.value)}
				    	required 
				    	className="form-control rounded  placeholder mb-2" />
				  </Form.Group>

				  <Form.Group className="mb-2 pl-0" controlId="stocks">
				  <Form.Label>Image</Form.Label>
				  <Form.Control type="src"  
				    	placeholder="Image" 
				    	value={src}
				    	onChange={ e => setSrc(e.target.value)}
				    	required 
				    	className="form-control rounded  placeholder mb-2" />
				  </Form.Group>

				  { isActive ? 
				  	<Button variant="primary" type="submit" id="submitBtn" className="rounded-0 float-right" >
				  	    Add
				  	</Button>
				  	:
				  	<Button variant="primary" type="submit" id="submitBtn" className="rounded-0 float-right" disabled>
				  	  Add
				  	</Button>
				   }

				</Form>
			</div>

		</div>
	</div>
	:
		<Redirect to="/products/all" />



		)
}


