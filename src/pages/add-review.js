import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect, useHistory, NavLink } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../user-context.js';
import productImage from '../images/product.png'; 
import Image from 'react-bootstrap/Image'
import { FaStar } from "react-icons/fa";
import Nav from 'react-bootstrap/Nav';
import { BiLeftArrowAlt } from "react-icons/bi";


const colors = {
    blue: " #007BFF",
    darkblue: "#051835"
 };

export default function Review(productProp) {

    const {user} = useContext(UserContext);
    const history = useHistory();

    const [ratingValue, setRatingValue] = useState(0);
    const [hoverValue, setHoverValue] = useState(undefined)
    const [comment, setComment] = useState('');
    const [isActive, setIsActive] = useState('');
    const productId = productProp.match.params.productId

 function  add(e) {

        e.preventDefault();

        fetch(`https://floating-beach-02947.herokuapp.com/products/${productId}/addReview`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ localStorage.token }`
            },
    		body: JSON.stringify({
    		    userId: user._id,
    		    rating:ratingValue,
    		    comment: comment,
    		    name: user.firstName + user.lastName
    								})
    		})
    		.then(res => res.json())
    		.then(data => {
    			console.log(data);
    			if(data === true){
    			    setRatingValue('');
    			    setComment('');

    			    Swal.fire({
    			        title: 'Thank you for your review',
    			        icon: 'success',
    			     
    			    							});
    			    history.push(`/products/${productId}`);

    			} 
    			else {
						Swal.fire({
    			        title: 'Something went  wrong',
    			        icon: 'error',
    			        text: 'Please try again.'   
    			    							});

    				};
    		});
       
}
const[product, setProduct]= useState("");
function getproduct(){

			fetch(`https://floating-beach-02947.herokuapp.com/products/${productId}`)
			.then (res=> res.json())
			.then(data =>{
				setProduct(data.product);
		
			})
		}

useEffect(() => {
		getproduct();
        // Validation to enable submit button when all fields are populated and both passwords match
        if(ratingValue !== 0 && comment !== '' ){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [ratingValue, comment]);

// STAR RATING--------------------


const stars = Array(5).fill(0)

const handleClick = value => {
  setRatingValue(value)
  console.log(value)
  console.log(ratingValue)
}

const handleMouseOver = newHoverValue => {
  setHoverValue(newHoverValue)
 
};

const handleMouseLeave = () => {
  setHoverValue(undefined)
}


	return( 
	(user.isAdmin === false) ?
	<div className="m-0 pt-5  pb-4  d-flex justify-content-center">
		
			<div className="col-sm-8 col-10 order-md-1 ">
				<Form className="mt-5  rounded shadow1 add-review-bg" onSubmit={(e) => add(e)}>
					<Nav.Link as={NavLink}  to={`/products/${productId}`}exact  >
					<h5 className=" text-right pr-4 align-bottom pt-4 go-back text-blue"><BiLeftArrowAlt />Go back </h5> </Nav.Link>
					<h1 className="montagu text-center text-white pt-5 pl-1 pr-1">Product Review</h1>
					<h4 className="poppins text-center text-white pt-0 pl-1 pr-1">{product.name}</h4>
					<div className="d-flex mb-3 float-center justify-content-center">
					  {stars.map((_, index) => {
					    return (
					      <FaStar
					        key={index}
					        size={24}
					        onClick={() => handleClick(index + 1)}
					        onMouseOver={() => handleMouseOver(index + 1)}
					        onMouseLeave={handleMouseLeave}
					        color={(hoverValue || ratingValue) > index ? colors.blue : colors.darkblue}
					        style={{
					          marginRight: 10,
					          cursor: "pointer"
					        }}
					      />
					    )
					  })}
					</div>

				<Form.Group className="mb-2 pl-3 pr-3 pb-4" controlId="comment">
				   <Form.Control as="textarea" rows={3}
				   placeholder="What is your feedback?" 
				   value={comment}
				   onChange={ e => setComment(e.target.value)}
				   required
				   className="form-control rounded placeholder mb-2"
				   style={{
				     backgroundColor: "#051835",
				     color: "#506FA9",
				     border:0
				   }}
				    />
				   { isActive ? 
				    	<Button variant="primary" type="submit" id="submitBtn" className="rounded float-right d-block" >
				    	    SUBMIT
				    	</Button>
				    	:
				    	<Button variant="primary" type="submit" id="submitBtn" className="rounded float-right d-block poppins" disabled>
				    	  SUBMIT
				    	</Button>
				     }
				 </Form.Group>	 
				 	 
				 	<p className="text-left col-12 pb-2 light-blue-text">We are glad to recieve your feedback.</p>
				</Form>
				
			</div>


	
	</div>
	:
		<Redirect to="/products/all" />



		)
}


