import {Fragment, useEffect, useState, useContext} from 'react';

import { Row, Col, Card, Button, Form, FormControl, NavDropdown }from 'react-bootstrap';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import OrderCard from '../components/order-card'
import UserContext from '../user-context';
import {Route, Link, NavLink, Redirect} from 'react-router-dom';
import { BsEmojiWink, BsClipboardCheck, BsChatSquareDots} from "react-icons/bs";
import Image from 'react-bootstrap/Image';
import productlogo from '../images/logo.png';


export default function Order(){

	const{user} =useContext(UserContext);
	const[isEmpty, setIsEmpty]= useState(false);
	const [orders, setOrders]=useState([]);
	const [countOrders, setCountOders]=useState(0);
	const [countProducts, setCountProducts]=useState(0);
	const [countReviews, setCountReviews]=useState(0);
	const [aveRating, setAveRating]=useState(0);
	const [countUsers, setCountUsers]=useState(0);
	const [countSubscribers, setCountSubscribers]=useState(0);


		
		console.log(user.isAdmin);
		console.log(localStorage.token)
useEffect(()=>{
	console.log(localStorage.token)
		UserCount()
		reviewCount()
		productCount()
		subscriberCount()
		fetch('https://floating-beach-02947.herokuapp.com/orders', {
			    headers: {
			        Authorization: `Bearer ${ localStorage.token }`
			    }
			    })
			.then (res=> res.json())
			.then(data =>{
				setCountOders(data.length);

				if (data !== '' || data !== false){
					setOrders(data.map(order =>{
							return(
								<OrderCard key= {order.id}orderProp ={order}/>
								)
							})
						);				
				}
				else{setIsEmpty(true)}
				
			})
		}, [])
function UserCount(){
	fetch('https://floating-beach-02947.herokuapp.com/users/all')
		.then (res=> res.json())
				.then(data =>{
					console.log(data.count);
					setCountUsers(data.count);
		})
}
function reviewCount(){
	fetch('https://floating-beach-02947.herokuapp.com/products/reviews/all')
		.then (res=> res.json())
				.then(data =>{
					setCountReviews(data.count);
					setAveRating(data.average);
		})
}
function subscriberCount(){
	fetch('https://floating-beach-02947.herokuapp.com/users/all/subscribers')
		.then (res=> res.json())
				.then(data =>{
					setCountSubscribers(data.count);
		})
}
	
function productCount(){
	fetch('https://floating-beach-02947.herokuapp.com/products/all')
		.then (res=> res.json())
				.then(data =>{
					setCountProducts(data.length);
		})
}
	return(

		<Col className=" p-0 m-0 vh-100 mb-0 pb-0">
			<div className="col-12 m-0 p-0 d-md-flex pt-2 mb-0 pb-0">
				<div className="col-12 col-md-6 m-0 p-0 pt-md-5  pb-0 mt-5 vh-50 vh-md-100 pl-2 pr-2">
					<div className="col-12 m-0 p-0 mb-2 p-0 pb-1 mb-0  shadow-1 rounded d-flex dark-blue">
						<div className="col-6  m-0 p-0 pt-5 text-white montagu align-middle">
						<h4 className="oblique mb-0 pb-0 pl-3 pt-2">You have</h4>
						<h1 className=" text-left count pl-4 user-count">{countProducts}</h1>
						<h4 className=" mb-0 pb-2 pl-5">products</h4>
						</div>
						<div className="container-fluid p-0 ">
							<Image  src={productlogo} fluid className="w-100 p-0 m-0"/>
						</div>


					</div>
					<div className="justify-content-around col-12 m-0 p-0 blue mb-2 pt-1  pb-1 mb-0 shadow-1 rounded d-flex dark-blue-text">
						<div className="text-center align-items-center d-lg-flex m-0 p-0 col-6 ">
							<h1 className="margin-auto col-lg-6 col-12 text-center count">{countUsers}</h1>
							<h5 className="margin-auto col-lg-6 col-12 text-lg-left text-center">USERS</h5>		
						</div>
						<div className="text-center align-items-center d-lg-flex m-0 p-0 col-6 ">
							<h1 className="margin-auto col-lg-6 col-12 text-center count">{countOrders}</h1>
							<h5 className="margin-auto col-lg-6 col-12 text-lg-left text-center">ORDERS</h5>		
						</div>
					</div>
					<div className=" dark-blue-text d-flex justify-content-around col-12 m-0 p-0 blue mb-0 pt-1 pb-1 mb-0  shadow-1 rounded align-middle">
					 <div className="text-center m-0 p-0 align-middle">
					 	<h1>{aveRating}</h1>
					 	<h5 className="">RATING</h5>
					 	
					 </div>
					 <div className="text-center m-0 p-0 align-middle">
					 	<h1>{countReviews}</h1>
					 	<h5 className="">REVIEWS</h5>
					 	
					 </div>
					 <div className="text-center m-0 p-0 align-middle">
					 	<h1>{countSubscribers}</h1>
					 	<h5 className="">SUBSCRIBERS</h5>
					 	
					 </div>
					</div>
				</div>
				<div className="col-12 col-md-6 m-0 p-0 pb-0 mt-5 pt-5 container-right pl-2 pr-2  vh-100 mb-0 ">
					<h2 className="montagu text-blue text-center ">Orders</h2>
					<Fragment className="mb-5 mb-sm-0">
						{orders}	
					</Fragment>
				</div >
			</div>
					
		</Col>
		)
		
	
}