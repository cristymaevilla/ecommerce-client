import {useState} from 'react';
import { Button , Row , Col}from 'react-bootstrap';
import { NavLink} from 'react-router-dom';
import Image from 'react-bootstrap/Image'
import error from '../images/error.jpg'; 


export default function NotFound(){
	
		return (
			<Row className="vh-100 m-0 p-0 d-flex justify-content-center">
				<Col className="p-5 pt-5 pr-5 d-flex-column margin-auto justify-content-center position-absolute top-50 start-50">
					<div className="container-fluid alin-middle error-img vh-25">
							<Image   src={error} fluid className=" error-img vh-25"/>
					</div>	
					
					<Button className="text-center float-center mt-4 " variant="primary" as={NavLink} to ="/" exact >Go Back to HomePage</Button>
				
					
				</Col>
			</Row>
			)
	
}





	

			


		

	