import {useState, useEffect, useContext } from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import UserContext from '../user-context';
import Image from 'react-bootstrap/Image';
import productImage from '../images/product.png';
import Swal from 'sweetalert2';
import { BiLeftArrowAlt } from "react-icons/bi";
import { FaCheck } from 'react-icons/fa';
import { NavLink, useHistory } from 'react-router-dom';
import { FaStar } from "react-icons/fa";
const colors = {
    blue: " #007BFF",
    darkblue: "#051835"
 };



 export default	function ProductView(productProp){
 		// const{_id, image, name, price, description}=productProp;
 	
// const{product} =useContext(ProductContext);
		const {user, setUser}=useContext(UserContext);
		const productId = productProp.match.params.productId
		const[product, setProduct]= useState("");	
		const[items, setItems]= useState(0);
		const[review, setReview]= useState("no reviews");
		const[averageRating, setAverageRating]= useState(0);
		const[isAdded, setIsAdded]= useState(false);
		const stars = Array(5).fill(0)
		 const history = useHistory();
		// const[product, setProduct]= useState("");
		useEffect(()=>{

			fetch(`https://floating-beach-02947.herokuapp.com/products/${productId}`)
			.then (res=> res.json())
			.then(data =>{
				console.log(data);
				setReview(data.Reviews);
				setProduct(data.product);
				setAverageRating(data.averageRating);
		
			})
		}, [])

function addReview(){
	console.log(user.id)
	if (user.id === null){
		Swal.fire({
			          title: `Oops! Sorry`,
			          icon: 'warning',
			          text: 'Kindly login first.'   
			      }); 
	}else{ 
history.push(`/${productId}/review`);}

	
}	


		return	(
				<Container className="mt-5 p-2">
						<Col> 
							<Card className=" mt-2 mt-sm-4">
									<Card.Body className="text-center text-xl-left p-0 d-sm-flex align-items-middle shadow1 product-view-bg rounded">

										<div className="container-fluid p-0 product-view-img col-12 col-sm-5">
											<Image  src={product.src} fluid className="w-100 p-0 m-0"/>
										</div>
										<div className="container-fluid p-0 product-view-img col-12 col-sm-7 pl-2 pr-2 pb-2 product-bg">
											<Nav.Link as={NavLink} to="/products/all" exact  >
											<h5 className=" text-right pr-4 align-bottom pt-4 go-back "><BiLeftArrowAlt />Go back </h5> </Nav.Link>
											<h1 className="pt-sm-3 mt-sm-3 montagu text-white">{product.name}</h1>
											<h4 className="text-white	">20 Tea Bags</h4>

											<Card.Text className="text-white	">{product.description}</Card.Text>
											<h2 className="text-white	">Php {product.price}</h2>
											<div className="d-sm-flex ">
												<Card.Text className="text-white pr-1 d-flex pl-2 pl-sm-0 mb-0">Rating:
												<span className="d-flex p-0 m-0 float-center justify-content-center align-items-middle">
												  {stars.map((_, index) => {
												    return (
												      <FaStar
												        key={index}
												        size={19}
												        color={averageRating > index ? colors.blue : colors.darkblue}
												        style={{
												          marginRight: 8,
												        }}
												      />
												    )
												  })}
												</span>
												</Card.Text>
												<Card.Text className=" text-white p-0 m-0 text-left pl-2 pl-sm-0">Reviews: {review}</Card.Text>
											</div>
											{user.isAdmin? 
													<></>
											:
												<Button className="text-center mt-4 " variant="primary" onClick={addReview} >Add Review</Button>
											}

										
								
											
											
										</div>
									</Card.Body>
							</Card>
						</Col>
				</Container>

			)	
 }

