
import {Fragment, useEffect, useState, useContext} from 'react';
import { GrAdd } from "react-icons/gr";

import ProductCard from '../components/product-card'
// import productsData from '../data/products-data';
import { Row, Col, Card, Button, Form, FormControl, NavDropdown }from 'react-bootstrap';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import ProductView from '../pages/product-view'
import {Route, Switch, Link} from 'react-router-dom';
import categoryData from '../data/category'
import UserContext from '../user-context';


export default function Products(){
	// const{id, name,  description}=categoryData

	// State that will be used to store the curses retrieved from the database
	const[products, setProducts]= useState([]);
	const {user, setUser}=useContext(UserContext);
	useEffect(()=>{
		console.log(user.Admin)
		viewAllProducts();

	}, [])
function viewAllProducts(){
			console.log(user.Admin)
			if(user.isAdmin===true){
				fetch('https://floating-beach-02947.herokuapp.com/products/all', {
			    headers: {
			        Authorization: `Bearer ${ localStorage.token }`
			    }
			    })
				.then (res=> res.json())
				.then(data =>{
					console.log(data);

					// sets the courses state toap the data retrieved from the fetch request in several components
					setProducts(data.map(product =>{
							return(
								// key is to check the Ids in courseData
								<ProductCard key= {product.id}productProp ={product}/>
								)
							})
						); 
					
				})
			}else{
				fetch('https://floating-beach-02947.herokuapp.com/products')
				.then (res=> res.json())
				.then(data =>{
					console.log(data);

					// sets the courses state toap the data retrieved from the fetch request in several components
					setProducts(data.map(product =>{
							return(
								// key is to check the Ids in courseData
								<ProductCard key= {product.id}productProp ={product}/>
								)
							})
						); 
					
				})
			}

		}

const[isCategorized, setIsCategorized]= useState(false);
const[index, setIndex]= useState(0);
	
function viewByCategory(i){
	fetch(`https://floating-beach-02947.herokuapp.com/products/category/${categoryData[i].param}`)
	.then (res=> res.json())
	.then(data =>{
		setIsCategorized(true);
			setProducts(data.map(product =>{
					return(<ProductCard key= {product.id}productProp ={product}/>)
					})
			)
		 }) 
		}
function backToAllProducts(){setIsCategorized(false);viewAllProducts();}

function greenTeaIndex(){setIndex(0);
	viewByCategory(index);}
function blackTeaIndex(){setIndex(1);
	viewByCategory(index);}
function fruitTeaIndex(){setIndex(2);
	viewByCategory(index);}
function herbalTeaIndex(){setIndex(3);
	viewByCategory(index);}
// views all  the products
	return(
		<Col className="pt-5 mt-2 mr-0 ml-0 pl-0 pr-0">
		<Navbar bg="primary"  className="justify-content-around rounded col-12 fixed d-flex">
		  <Navbar.Brand onClick={greenTeaIndex} className="category-hover texthover text-white">Green
		  <span className="d-none d-md-inline texthover ">Tea</span>
		  </Navbar.Brand>
		   <Navbar.Brand onClick={blackTeaIndex} className="category-hover texthover text-white">Black
		   <span className="d-none d-md-inline texthover">Tea</span>
		   </Navbar.Brand>
		    <Navbar.Brand onClick={fruitTeaIndex} className="category-hover texthover text-white">Fruit
		    <span className="d-none d-md-inline texthover">Tea</span>
		    </Navbar.Brand>
		     <Navbar.Brand onClick={herbalTeaIndex} className="category-hover texthover text-white">Herbal 
		     <span className="d-none d-md-inline texthover">Tea</span>
		     </Navbar.Brand>  
		</Navbar>	
		{(isCategorized)? 
			<Row >
				<Card style={{ width: '18rem' }} className=" col-11 center pl-0 pr-0 mt-3 ">
				  <Card.Body className=" d-sm-flex header shadow2 rounded">
				  	<div className=" d-flex-column col-12 col-sm-6 text-left">
				  		<h1 className="col-12 montagu">{categoryData[index].name} </h1>
				  		<h2 className="col-12 "> <em>tea</em></h2>
				  		<Button variant="primary" className="d-sm-block align-bottom d-none " onClick={backToAllProducts}>All Products</Button>
				  	</div>
				    <p className="col-12 col-sm-6 text-center align-middle poppins">{categoryData[index].description}</p>
				  </Card.Body>
				  <Button variant="primary" className="d-sm-none" onClick={backToAllProducts} >All Products</Button>
				</Card>
			</Row>
				:
				<div></div>
			}
		<Row className=" dark-blue-text mt-5 mb-3">
			<Fragment>
				{products}	
			</Fragment>
		</Row>
		{(user.isAdmin)?
				<Link className	="none fixed-bottom bg-primary rounded-circle float-right  mb-4 ml-4 align-items-middle margin-auto zoom-out add-product text-center" to={"/products/add"} exact>
					 <GrAdd/>
				</Link>
		:
			<></>
		}	
		</Col>
		
		)
}

