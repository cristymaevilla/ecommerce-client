import { useState } from "react";
import { FaStar } from "react-icons/fa";

const colors = {
    blue: " #007BFF",
    grey: "#a9a9a9"
    
};

export default function Rating() {
  const [currentValue, setCurrentValue] = useState(0);
  const [hoverValue, setHoverValue] = useState(undefined);
  const stars = Array(5).fill(0)

  const handleClick = value => {
    setCurrentValue(value)
    console.log(value)
  }

  const handleMouseOver = newHoverValue => {
    setHoverValue(newHoverValue)
    console.log(newHoverValue)
  };

  const handleMouseLeave = () => {
    setHoverValue(undefined)
  }


  return (

  	
    <div className="d-flex-column align-items-center justify-content-center">
      <h2> PRODUCT NAME </h2>
      <div className="d-flex-row ">
        {stars.map((_, index) => {
          return (
            <FaStar
              key={index}
              size={24}
              onClick={() => handleClick(index + 1)}
              onMouseOver={() => handleMouseOver(index + 1)}
              onMouseLeave={handleMouseLeave}
              color={(hoverValue || currentValue) > index ? colors.blue : colors.grey}
              style={{
                marginRight: 10,
                cursor: "pointer"
              }}
            />
          )
        })}
      </div>
      <textarea
        placeholder="What is your feedback?"
      />

      <button>
        Submit
      </button>
      
    </div>
  );
};


