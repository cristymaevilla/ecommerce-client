import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../user-context.js';

import Image from 'react-bootstrap/Image'
import img from '../images/text.JPG'; 

export default function UpdateProduct(productProp) {

    const {user} = useContext(UserContext);
    const history = useHistory();

    // State hooks to store the values of the input fields
    const productId = productProp.match.params.productId

    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [category, setCategory] = useState('');
    const [price, setPrice] = useState('');
    const [stocks, setStocks] = useState('');
    const [src, setSrc] = useState('');
    const [isActive, setIsActive] = useState('');
   
	useEffect(()=>{

			fetch(`https://floating-beach-02947.herokuapp.com/products/${productId}`)
			.then (res=> res.json())
			.then(data =>{
				console.log(data);
	
				setName(data.product.name)
				setDescription(data.product.description);
				setCategory(data.product.category);
				setPrice(data.product.price);
				setStocks(data.product.stocks);	
				setSrc(data.product.src);
			})
		}, [])

    // Check if values are successfully binded
 function  update(e) {

        e.preventDefault();

        fetch(`https://floating-beach-02947.herokuapp.com/products/${productId}/update`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ localStorage.token }`
            },
    		body: JSON.stringify({
    		    name: name,
    		    description: description,
    		    category: category,
    		    price:price,
    		    src: src,
    		    stocks: stocks
    								})
    		})
    		.then(res => res.json())
    		.then(data => {
    			if(data === true){
    			    setName('');
    			    setDescription('');
    			    setCategory('');
    			    setPrice('');
    			    setStocks('');
    			    setSrc('');
    				console.log(category);
    			    Swal.fire({
    			        title: 'Product successfully updated',
    			        icon: 'success',
    			    							});
    			    history.push("/products/all");

    			} 
    			else {
						Swal.fire({
    			        title: 'Something went wrong',
    			        icon: 'error',
    			        text: 'Please try again.'   
    			    							});

    				};
    		});
       
}

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(name !== '' && description !== '' && category !== ''  && src !== '' && price !== '' && stocks !== ''){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, description,category,  price, stocks]);
	
	return( 
	(user.isAdmin === true) ?
	<div className="m-0 pt-5  pb-5  col-12 pr-0 pl-0  d-flex vw-100 updateproduct-bg d-flex justify-content-center">
			
			<div className=" d-flex justify-content-center col-11 col-lg-6 p-3 pl-4 pr-4 ml-4 mr-5 float-center rounded  pb-5">

				<Form className="mt-5 col-md-10 col-11 text-center " onSubmit={(e) => update(e)}>
				<h3 className="montagu"> PRODUCT UPDATE</h3>
				  <Form.Group className="mb-2 pl-0" controlId="name">
				    <Form.Label>Name</Form.Label>
				    <Form.Control type="name"
				    	 placeholder="name" 
				    	 value={name}
				    	 onChange={ e => setName(name)}
				    	 required
				    	 className="form-control rounded placeholder mb-2" />
				  </Form.Group>
				  
				  <Form.Group className="mb-2 pl-0" controlId="description">
				     <Form.Label>Description</Form.Label>
				     <Form.Control as="textarea" rows={3}
				     placeholder="description" 
				     value={description}
				     onChange={ e => setDescription(e.target.value)}
				     required
				     className="form-control rounded placeholder mb-2"
				      />
				   </Form.Group>

				<Form.Group controlId="exampleForm.ControlSelect1">
				    <Form.Label>Category</Form.Label>
				  <Form.Control as="select"
				  	placeholder="category" 
				  	value={category}
				  	onChange = { e => setCategory(e.target.value)}
				  	required 
				  	className="form-control rounded placeholder mb-2"
				  >
				    <option>green tea</option>
				    <option>black tea</option>
				    <option>herbal tea</option>
				    <option>fruit tea</option>
				  </Form.Control>
				</Form.Group>

				  <Form.Group className="mb-2 pl-0" controlId="price">
					<Form.Label>Price</Form.Label>
				    <Form.Control type="price"  
				    	placeholder="Price" 
				    	value={price}
				    	onChange={ e => setPrice(e.target.value)}
				    	required 
				    	className="form-control rounded  placeholder mb-2" />
				  </Form.Group>

				  <Form.Group className="mb-2 pl-0" controlId="stocks">
				  <Form.Label>Stocks</Form.Label>
				  <Form.Control type="stocks"  
				    	placeholder="Stocks" 
				    	value={stocks}
				    	onChange={ e => setStocks(e.target.value)}
				    	required 
				    	className="form-control rounded  placeholder mb-2" />

				  </Form.Group> 

				  <Form.Group className="mb-2 pl-0" controlId="stocks">
				  <Form.Label>Image</Form.Label>
				  <Form.Control type="src"  
				    	placeholder="Image" 
				    	value={src}
				    	onChange={ e => setSrc(e.target.value)}
				    	required 
				    	className="form-control rounded  placeholder mb-2" />

				  </Form.Group>
				  	<Button variant="primary" type="submit" id="submitBtn" className="rounded float-right mb-5" >
				  	    Update </Button>
				  	
				

				</Form>
			</div>

	
	</div>
	:
		<Redirect to="/products/all" />



		)
}


