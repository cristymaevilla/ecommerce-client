import {Fragment, useEffect, useState, useContext} from 'react';

import { Row, Col, Card, Button, Form, FormControl, NavDropdown, Container }from 'react-bootstrap';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import ReviewCard from '../components/review-card'
import UserContext from '../user-context';
import {Route, Link, NavLink, Redirect} from 'react-router-dom';
import { BsEmojiWink, BsClipboardCheck, BsChatSquareDots} from "react-icons/bs";
import { HiOutlineTicket } from "react-icons/hi";
import Image from 'react-bootstrap/Image';
import banner1 from '../images/banner1.jpg';
import banner2 from '../images/banner2.jpg';


import rev from './add-review';

export default function Order(){
	const{user} =useContext(UserContext);
	const[isReviewsEmpty, setIsReviewsEmpty]= useState(false);
	const[showReview, setShowReview]= useState(false);
	const [reviews, setReviews]=useState([]);
	const [userDetails, setUserDetails]=useState('');

useEffect(()=>{
		
	fetch('https://floating-beach-02947.herokuapp.com/users/details', {
		    headers: {
		        Authorization: `Bearer ${ localStorage.token }`
		    }
		    })
		.then (res=> res.json())
		.then(data =>{
			console.log(data);
			setUserDetails(data);		
		})
	}, []) 


function getReviews(){
	setShowReview(true)	
	fetch('https://floating-beach-02947.herokuapp.com/products/reviews/user', {
		    headers: {
		        Authorization: `Bearer ${ localStorage.token }`
		    }
		    })
		.then (res=> res.json())
		.then(data =>{
			console.log(showReview);

			if (data !== '' || data !== false){
				setIsReviewsEmpty(false)
				setReviews(data.map(review =>{
						return(
							<ReviewCard key= {review.id}reviewProp ={review}/>
							)
						})
					);				
			}
			else{setIsReviewsEmpty(true)}
			
		})
	}
	
	return(
	<Col className=" p-0 m-0 vw-100 mb-0 pb-0 pt-5">
		<div className=" p-0 m-0  ml-2 mr-2  mb-2 nav-dashboard  ">
			<div className=" p-0 m-0 pt-1 pb-1 mt-5 d-sm-flex justify-content-around  rounded">
				<div className="col-sm-6 pl-1 pl-sm-3 pl-xl-4">
					<h3 className=" poppins dark-blue-text">Hi there</h3>
					<h1 className="name montagu text-blue">{user.firstName}</h1>
				</div>
				<div className="col-sm-6 d-flex justify-content-around align-middle text-center text-blue">
					<div className="">
						<div className="user-icons container-fluid p-0 m-0"><HiOutlineTicket/></div>
						<div className="poppins">Voucher</div>
					</div>
					<Nav.Link className="p-0 m-0" as={NavLink} to="/profile/orders" exact>
						<div className="user-icons container-fluid p-0 m-0"> <BsClipboardCheck/></div>
						<div className="poppins" >Orders</div>
					</Nav.Link>
					<div className="">
						<div className="user-icons container-fluid p-0 m-0" onClick={getReviews}><BsChatSquareDots/></div>
						<div className="poppins" >Reviews</div>
					</div>
				</div>
			</div>
		</div>
		<div className="col-12 m-0 p-0  pt-2 pb-0  align-items-center mb-4">
			<div className="col-12  m-0 p-0 pt-md-2 pb-md-3 pb-0 mt-0  ">
				<div className=" m-0 p-0 blue mb-0 pt-0  shadow-1 rounded container-fluid user-bg  ">
				
					<Image  src={banner1} fluid className="w-100 p-0 m-0 rounded d-block d-sm-none "/>
					<Image  src={banner2} fluid className="w-100 p-0 m-0 rounded d-none d-sm-block"/>
										
				</div>
			</div>
			<div className="col-12  m-0 p-0 pb-5 mt-1  pl-2 pr-2 vh-50 vh-md-100">
			{showReview ?  
				<>
				<h1 className="text-center montagu text-blue">My Reviews</h1>
					{isReviewsEmpty?
						<h1 className="text-center montagu text-blue">You haven't reviewed our products yet</h1>
					:
						<Row className="mb-5 mb-sm-0 col-12 d-sm-flex justify-content-center margin-auto ml-0 mr-0">
							<Fragment>
								{reviews}	
							</Fragment>
						</Row>
					}		
				</>
			:
				<>
				<h1 className="text-center montagu text-blue">Profile</h1>
				<div className=" poppins dark-blue-text">
					<h5>Name : {userDetails.firstName} {userDetails.lastName}</h5>
					<h5>Email : {userDetails.email}</h5>
					<h5>Mobile No : {userDetails.mobileNo}</h5>
					<h5>Address: {userDetails.completeAddress}</h5>
				</div>
				</>

			}	
				
			</div >
		</div>
				
	</Col>
				
	)
}
