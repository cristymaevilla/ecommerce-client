import {Fragment, useEffect, useState, useContext} from 'react';

import { Row, Col, Card, Button, Form, FormControl, NavDropdown, Container }from 'react-bootstrap';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import OrderCard from '../components/order-card'
import UserContext from '../user-context';
import {Route, Link, NavLink, Redirect} from 'react-router-dom';
import { BsBackspace} from "react-icons/bs";
import { BsEmojiWink, BsClipboardCheck, BsChatSquareDots} from "react-icons/bs";
import { HiOutlineTicket } from "react-icons/hi";
import Image from 'react-bootstrap/Image';

import rev from './add-review';

export default function Order(){
	const{user} =useContext(UserContext);
	const[isEmpty, setIsEmpty]= useState(false);
	const [orders, setOrders]=useState([]);
	const [title, setTitle]=useState("My Orders");
	const [isActive1, setIsActive1]=useState(false);
	const [isActive2, setIsActive2]=useState(false);
	const [isActive3, setIsActive3]=useState(false);
	const [showDeliveredOrders, setShowDeliveredOrders]=useState(false);
	const [showAllOrders, setShowAllOrders]=useState(true);
	const [showToShipOrders, setShowToShipOrders]=useState(false);

useEffect(()=>{	
	if(showAllOrders === true && showDeliveredOrders === false && showToShipOrders === false){allOrders();}
	if(showAllOrders === false && showDeliveredOrders === true && showToShipOrders === false){deliveredOrders();}
	if(showAllOrders === false && showDeliveredOrders === false && showToShipOrders === true){toShipOrders();}

	}, [])

function allOrders(){
	setIsActive1(false);setIsActive2(false);setIsActive3(true);
	setShowAllOrders(true);	
	setShowToShipOrders(false);
	setShowDeliveredOrders(false);
	fetch('https://floating-beach-02947.herokuapp.com/orders/users/orders', {
		    headers: {
		        Authorization: `Bearer ${ localStorage.token }`
		    }
		    })
		.then (res=> res.json())
		.then(data =>{
			console.log(data);

			if (data !== '' || data !== false){
				setTitle("My Orders")
				setOrders(data.map(order =>{
						return(
							<OrderCard key= {order.id}orderProp ={order}/>
							)
						})
					);				
			}
			else{setTitle("No Orders Yet"); setOrders("");}
			
		})
	}

function deliveredOrders(){
	setIsActive1(false);setIsActive2(true);setIsActive3(false);
	setShowAllOrders(false);	
	setShowToShipOrders(false);
	setShowDeliveredOrders(true);		
	fetch('https://floating-beach-02947.herokuapp.com/orders/users/delivered', {
		    headers: {
		        Authorization: `Bearer ${ localStorage.token }`
		    }
		    })
		.then (res=> res.json())
		.then(data =>{
			console.log(data);

			if (data !== false){
				setTitle("Delivered Orders")
				setOrders(data.map(order =>{
						return(
							<OrderCard key= {order.id}orderProp ={order}/>
							)
						})
					);				
			}
			else{setTitle("No delivered orders"); setOrders("");}
			
		})
	}

function toShipOrders(){
	setIsActive1(true);setIsActive2(false);setIsActive3(false);
	setShowAllOrders(false);	
	setShowToShipOrders(true);
	setShowDeliveredOrders(false);		
	fetch('https://floating-beach-02947.herokuapp.com/orders/users/to-ship', {
		    headers: {
		        Authorization: `Bearer ${ localStorage.token }`
		    }
		    })
		.then (res=> res.json())
		.then(data =>{
			console.log(data);
			if (data !== false){
				setTitle("To Ship Orders")
				setOrders(data.map(order =>{
						return(
							<OrderCard key= {order.id}orderProp ={order}/>
							)
						})
					);				
			}
			else{setTitle("No to ship orders"); setOrders("");}
			
		})
	}
const style= {
	color:"#007BFF",
	backgroundColor: "white",
	border: "#007BFF 1px solid"

}
	return(
	<Col className=" p-0 m-0 vw-100 mb-0 pb-0 pt-5">
		<div className=" p-0 m-0  ml-2 mr-2  mb-2 nav-dashboard  ">
			<div className=" p-0 m-0 pt-1 pb-1 mt-5 d-sm-flex justify-content-around  rounded">
				<div className="col-sm-6 pl-1 pl-sm-3 pl-xl-4">
					<h3 className=" poppins dark-blue-text">Hi there</h3>
					<Nav.Link className="p-0 m-0" as={NavLink} to="/profile" exact>
						<h1 className="name montagu text-blue">{user.firstName}</h1>
					</Nav.Link>
					
				</div>
				<div className="col-sm-6 d-flex  align-middle text-right">
					<Nav.Link className=" container-fluid p-0 m-0 dark-blue-text go-back" as={NavLink} to="/profile" exact>
						<h4 className="poppins"><BsBackspace/> Go Back</h4>
					</Nav.Link>

				</div>
			</div>
		</div>
		<div className="col-12 m-0 p-0  pt-2 pb-0  align-items-center mb-4">
			<div className="col-12  m-0 p-0 pt-md-2 pb-md-3 pb-0 mt-0  ">

				<div className="d-flex justify-content-center">
					<p className="col-sm-2 col-4 ml-1 blue text-center rounded p-1 p-sm-2 sub-link" style={isActive1? style : null } onClick={toShipOrders}>To Ship</p>
					<p className="col-sm-2 col-4 ml-1 blue text-center rounded p-1 p-sm-2 sub-link" style={isActive2? style : null } onClick={deliveredOrders}>Delivered</p>
					<p className="col-sm-2 col-4 ml-1 blue text-center rounded p-1 p-sm-2 sub-link" style={isActive3? style : null } onClick={allOrders}>All</p>							
				</div>
			</div>
			<div className="col-12  m-0 p-0 pb-5 mt-1  pl-2 pr-2 vh-50 vh-md-100">
				<h1 className="text-center montagu text-blue">{title}</h1>
				<Fragment className="mb-5 mb-sm-0">
					{orders}	
				</Fragment>
			</div >
		</div>
				
	</Col>
				
	)
}
